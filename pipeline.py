from cinco import Pipeline, Step

@Step
def a_step():
    print('This is pipeline step a')


@Step
def b_step():
    print('This is pipeline step b')

pipeline = Pipeline([a_step, b_step])
